# Exercise 1: Optimize the mass window: expected/observed significance

We will first try to find the mass window the optimizes the significance for a counting experiment. In this exercise, use Poisson counting and the original histograms with the 200 MeV bins.

## Code you could use form the skeleton code:

- `IntegratePoissonFromRight()` - small helper routine
- `Significance Optimization()` - start for the code

### Task a

Find the mass window the optimizes the expected significance. Make a plot of the significance as a function of the width of the mass window around 125 GeV and explain the structure you see.

### Task b

Find the mass window that optimizes the observed significance. And promise to never do that again.

### Task c

Find the mass window that optimizes the expected significance for a 5 times higher luminosity.

### Task d

At what Luminosity do you expect to be able to make a discovery? Note: expected significance more than 5 sigma.
