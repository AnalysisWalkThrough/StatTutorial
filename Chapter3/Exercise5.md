# Exercise 5: Create toy data-sets

### Task a

Write a routine that generates a toy data-set from MC templates. How: take the histogram h massApologies for the cutoff. Here's the continuation:

**Exercise 5: Create toy data-sets**
```markdown
# Exercise 5: Create toy data-sets

### Task a

Write a routine that generates a toy data-set from MC templates. How: take the histogram h mass template and draw a Poisson random number in each bin using the bin content as central value. The routine should return the full fake data-set (histogram).

### Task b

Generate 1000 toy data-sets for background-only, compute for each the test-statistic using the routine from Exercise 4 and plot the test statistic distribution. Then do the same for 1000 toy data-sets for the signal+background hypotheses.

### Task c

Plot both distributions in a single plot and indicate the value of the test-statistic in the original ’real’ data-set.
