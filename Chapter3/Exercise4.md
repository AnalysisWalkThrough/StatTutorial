# Exercise 4: Compute the test statistic

For each data-set we can compute the Likelihood Ratio test statistic. We take here the simplest form of the likelihood ratio test-statistic: L(µ = 1) X = −2ln(Q), with Q = L(µ = 0) for each of the two hypotheses we compute the Likelihood as (use α = 1): −2log(L) = −2(cid:88) log(Poisson(N | µ·fbin +α·fbin)) bins

### Task a

Write a routine that computes the likelihood ratio test-statistic for a given data-set (h mass dataset) from the expected ’template’ distributions from the background and the signal, also histograms:

### Task b

Compute X , the value of the likelihood ratio test-statistic for the data.
