# Exercise 6: Discovery-aimed: compute p-values

### Task a

Compute the p-value or 1−CL (under the b-only hypothesis):
- For the average (median) b-only experiment
- For the average (median) s+b experiment [expected significance]
- For the data [observed significance]

### Task b

Draw conclusions:
- Can you claim a discovery with this ’real’ data-set ?
- Did you expect to make a discovery ?
- At what luminosity do you expect to be able to make a discovery ?
