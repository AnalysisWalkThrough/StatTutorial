# Exercise 2: Data driven background estimate - sideband fit

Although the Monte-Carlo prediction for the background looks ok, we can actually try to estimate the background normalisation, by determining the scalefactor (α) by fitting the level of background in a signal-free region (a side-band). That will allow you to get a more accurate prediction for the background in the region where there is actually a signal present.

## Code you could use form the skeleton code:

- `SideBandFit()`

### Task a

Perform a likelihood fit to the side-band region 150 ≤ m ≤ 400 GeV to find the optimal scale-factor for the background and it’s uncertainty (α ± ∆α) ? Compute and plot −2ln(L), with the likelihood given by:

### Task b

Use the best estimate of the background scale factor and it’s uncertainty to predict the level of background (b±∆b) in a 10 GeV window around 125 GeV. You can also use the optimal window that your found in question 1.

### Task c

Compute the expected and observed significance using this new background estimate. Compare to those in Exercise 1 and discuss the differences.
