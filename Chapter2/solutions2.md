## Solutions

### Exercise 2: Data driven background estimate - sideband fit

1.  Use function `SideBandFit(1)`.\
    Background scale factor from sideband fit:
    $\alpha  =  1.11_{-~0.06}^{+~0.07}$

2.  Propagate scale factor from exercise a) to the mass window.\
    SM background in mass window: (width default masswindow = 10.00
    GeV):

    unscaled:  $` N_{\rm bgr} `$ = 6.42\
    scaled:  $` N_{\rm bgr} `$ = $7.10_{-~0.40}^{+~0.42}$


3.  The solutions can also be found when running `SideBandFit(1)` \
    In a 10 GeV mass window we expect from background (b=6.42/7.10
    events: unscaled/scaled), from signal (s=5.96) and in data we
    observe (d=16) events.

    **option 1:** assuming no bkg uncertainty and scaling ( $b = 6.42, \Delta b = 0.00$ ):\
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; p_value = 3.12e-02 $\rightarrow$ 1.86 sigma.

    **option 2:** āssuming scaled bkg with bkg uncertainty ($b = 7.10, \Delta b = 0.41$):\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; p_value = 3.13e-02 $\rightarrow$ 1.86 sigma.

    In this case with small bkg uncertainty and small number of events,
    the uncertainty has very little impact. Try to see what happens at
    larger luminosities.

### Exercise 3: Measurement of the production cross-section

We run the fit using 2 GeV bins, i.e. use a rebin factor of 10.

1.  Run `MuFit(10,1)`.

    Best fit: $\alpha$ = 1.10 , $\mu$ = 1.28

2.  Run `MuFit(10,2)`.

    We should 'profile' the uncertainty in $\alpha$. Just in case, we
    also show the value if we would just look at the slice at the best
    value of $\alpha$.\
    Result on mu:

    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; best alpha approach: $\mu = 1.29_{-~0.54}^{+~0.66}$\
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; profiled: $\mu = 1.27_{-~0.54}^{+~0.66}$

    Not a strange result as the variables are not so much correlated.
    Let me point out here that in the real Higgs analysis the signal
    scale factor is strongly correlated with the actual mass as the
    production cross-section and branching fraction of the Higgs to 4
    muons depends on the mass of the Higgs boson.
