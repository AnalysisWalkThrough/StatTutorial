# Exercise 7: Exclusion-aimed: compute CLs+b

### Task a

Compute the CLs+b:
- For the average (median) s+b experiment
- For the average (median) b-only experiment [expected CLs+b]
- For the data [observed CLs+b]

### Task b

Draw conclusions: We can try to see if we can exclude the m =125 GeV hypothesis. As that is a yes/no answer only, we can also try to estimate what scale factor of the Higgs boson production cross-section (relative the the SM prediction) we can exclude or were expected to be able to exclude.
- Can you exclude the m =125 GeV hypothesis ?
- What cross-section scale factor can we exclude ?
- Did you expect to be able to exclude the m =125 GeV hypothesis ?
- What cross-section scale factor did you expect to be able to exclude ?
