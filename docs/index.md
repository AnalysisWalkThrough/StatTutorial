# StatTutorial

Welcome! In this tutorial, you will learn about a few of the statistical techniques that are essential in analyzing the data obtained from the proton-proton collision experiments.

The tutorial covers a wide range of topics, including likelihood fits, significances, exclusion limits and many more. You will be introduced to the basic concepts of each topic and provided with step-by-step instructions on how to actually perform the analysis using the ROOT software package.

The aim of this tutorial is to equip you with the necessary skills to analyze proton-proton collision data with confidence. So, let's get started!

# Setup Instructions

This document will guide you through the process of setting up the environment needed to run the Statistics Tutorial based on the ROOT framework.

## Prerequisites

- Access to a terminal/command line interface.
- Git installed on your machine. If not, you can download it from [here](https://git-scm.com/downloads).
- ROOT installed. You can download it from [here](https://root.cern/install/).

**Optional but recommended:**
- A modern IDE (Integrated Development Environment) that supports C++ [VS Code](https://code.visualstudio.com/)

## Remote setup
- In case you can not setup ROOT on your local machine, you can use a remote machine. For the Terascale Statistics school you can use ssh to connect. 

```
ssh -Y schoolXX@naf-school.desy.de
```
with XX=[00,80]. Keep your number in mind, and use this today and tomorrow.

The password will be provided during the school.

# Clone the Repository

First, clone the GitLab repository by running the following command in your terminal:

```shell
git clone https://gitlab.cern.ch/AnalysisWalkThrough/StatTutorial.git
```

To setup the right plot styles a rootlogon file can be setup using:
```
source setup.sh
```

## Optional in case ROOT can not be installed: Setup Docker and load the Docker image

If you do not have Docker installed, you can find installation instructions [here](https://docs.docker.com/get-docker/).

If you have Docker installed, you can run the following command from the root of the repository to get a terminal with the code of this repo and ROOT installed.

```console
docker run -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix --mount type=bind,source="$(pwd)",target=/opt/StatTutorial --rm -it --user $(id -u) rootproject/root:latest /bin/bash
```

The ```-e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix``` can be used on linux to enable graphical forwarding through the docker container. Fow windows and osx devices, see the `enabling graphics` section in https://hub.docker.com/r/rootproject/root.
