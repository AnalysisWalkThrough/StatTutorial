# Exercise 6: Discovery-aimed: compute p-values

### Exercise 6a)

Using the distributions for the test-ststistics compute the p-value or (1−CL), under the background-only hypothesis:

- For the average (median) b-only experiment
- For the average (median) s+b experiment [expected significance]
- For the data [observed significance]

### Exercise 6b)

Let's take it one step further and ask the following questions:

- Can you claim a discovery with this ’real’ data-set ?
- Did you expect to be able to make a discovery ?
- At what luminosity do you expect to be able to make a discovery ?
