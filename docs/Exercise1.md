# Exercise 1: Optimize the mass window

In this exercise we will analyse the four lepton mass distribution in a region around the Higgs boson peak. The goal is to maximize our sensitivity to a possible Higgs boson signal. At this moment we know of course that the Higgs boson exists, but for this exercise let's pretend we're in 2011 and we're curious to see there is a 125 GeV Higgs boson hidden in the data or not. We will first try to find the mass window the optimizes the significance for a counting experiment and go from there. In this exercise we use Poisson counting and the original histograms with the 200 MeV bins.

Addressing each of the questions requires that you can define a mass window and use the histograms to obtain the number of events in the window for data, SM background and the Higgs boson.

### Code to help you : helper routine and a small skeleton:

Throughout the course there are a few (small) 'helper routines' that might be useful. In this case a function to integrate part of a Poisson distribution and a small skeleton to get you started on the mass window.

- `IntegratePoissonFromRight()` - small helper routine
- `Significance_Optimization()` - small skeleton to get started


### Exercise 1a) optimising the expected significance
Find the mass window that optimizes (maximizes) the *expected* significance. Make a plot of the significance as a function of the width of the mass window around 125 GeV and explain the structure you see.

### Exercise 1b) do something you should not do
Find the mass window that optimizes the *observed* significance. And promise to never do that again!

### Exercise 1c) increasing the integrated luminosity
Find the mass window that optimizes the expected significance for a 5 times higher integrated luminosity, i.e. when the number of events from all hypotheses (SM and the Higgs boson) are scaled by a factor five. Discuss why this would be different than the answer in 1a).

### Exercise 1d) expected moment of discovery
At what level of integrated luminosity do you expect to be able to make a discovery? Note:
this is the point where the expected significance reaches 5 sigma.
