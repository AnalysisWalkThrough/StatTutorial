# Exercise 5: Create toy data-sets

#### Code you could use form the skeleton code:

- `GenerateToyDataSet()`


### Exercise 5a)

Write a routine that generates a toy data-set from a simulated templates (histogram h_mass_template). 

How? Take the histogram 'h_mass_template' and draw a Poisson random number in each bin using the bin content as central value and put that number in a new histogra. The routine should return that full fake data-set histogram.


### Exercise 5b)

Generate 1000 toy data-sets based on background-only, compute for each the test-statistic using the routine from Exercise 4 and plot the test statistic distribution. Then do the same for 1000 toy data-sets for the signal+background hypotheses.

### Exercise 5c)

Plot both distributions in a single plot and indicate the value of the test-statistic in the original ’real’ data-set. 

In exercise 6 we will try to interpret this value in terms of the (in)compatibility with the two hypotheses in a way similar as we did using the Poisson distributions in teh counting experiment from Chapter 1.


