# Exercise 4: Compute the test statistic

For each data-set we can compute the Likelihood ratio test statistic, defined as 

$X = −2{\rm Log}(Q)$, with $Q = \frac{L(µ = 1)}{L(µ = 0)}$ 

, where the likelihood is the same as we have used in previoous exercises: 

${\rm -2  \cdot Log(Likelihood)} = -2\sum_{\rm bins} {\rm Log(Poisson(n |   \alpha \cdot f_{\rm SM}) + \mu \cdot f_{\rm Higgs} )}$. 

Note that in this exercise we will not scale the background, so $\alpha=1.00$. Also note that we do not  compute the likelihood directly, but -2Log(Likelihood) and it is also convenient to use the fact that log(a)/log(b) = log(a)-log(b).

#### Code you could use form the skeleton code:

- `Get_TestStatistic()`

### Exercise 4a)

Write a function that computes the likelihood ratio test-statistic for a given data-set (h_mass_dataset) from the expected ’template’ distributions for the background and the signal, also histograms. You can start with the routing Get_TestStatistic() in the skeleton code.

Note: we will use this routine extensively in Exercise 5, so ask the function to return the  value of the test statistic.

### Exercise 4b)

Use the function in exericse 4a) to compute:

- Compute $X_{\rm data}$ , the value of the likelihood ratio test-statistic for the data
- Compute $X_{\rm data}$ , the value of the likelihood ratio test-statistic for the SM hypothesis histogram
- Compute $X_{\rm data}$ , the value of the likelihood ratio test-statistic for the SM+Higgs hypothesis histogram
