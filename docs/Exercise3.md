# Exercise 3: Measurement of the production cross-section

Using again the parametrisation of the expected background and signal yields: $f{\rm total} =  \mu \cdot f_{\rm Higgs} + \alpha \cdot f_{\rm SM}$, we can try to get an estimate of the Higgs cross-section scale factor. In Exercise 2 we only varied the background scale factor in the side-bands. Now we’ll perform a fit (side-band and signal region together) and extract the values and uncertainties of α and µ simultaneously. If the particle is not present in the data µ will be close to 0, if the signal is there µ will be close to 1.

### Exercise 3a)

Fix the background to the factor α you determined in exercise 2. Do a likelihood fit to the full mass range, where you leave only the cross-section scale factor for the signal free. What is the best value for µ? And what is the associated uncertainty?

### Exercise 3b)

Do a likelihood fit where you leave both the scale factor for the signal and that for the background free: a simultaneous fit. What is the best value for µ and α?

### Exercise 3c)
Try to make a 2d plot of the likelihood as a function of µ and α. How would you extract the uncertainties on µ and α ?
